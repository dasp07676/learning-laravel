<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Users;
use App\Http\Controllers\UserController;
use App\Http\Controllers\new_controller;
use App\Http\Controllers\html_controller;
use App\Http\Controllers\database_controller;
use App\Http\Controllers\login_controller;
use App\Http\Controllers\upload_controller;
use App\Http\Controllers\friends_controller;

Route::get('/welcome', function () {
    return view('welcome');
});

Route::get('/hello', function () {
    return view('hello');
});

// How to call a controller
// Route::get("users/{user}",[Users::class,'index']);

// Show data from view
Route::get("users/{name}", function($name) {
    return view('users', ['name'=>$name]);
});

// Show data from controllers
Route::get("/user/{name}", [UserController::class,'loadView']); 

Route::view("newuser", "newuser");

Route::get("/blade", [UserController::class, 'viewLoad']);

Route::get("new/{work}", [new_controller::class, 'newFileFunction']);

// passing data into the form
Route::view("login", "html_form");
Route::post("data", [html_controller::class,'getData']);

Route::view("home", "home");
Route::view("about", "about"); 
Route::view("sorry", "not_allowed");

// Fetching data from an existing database
Route::get("/database", [database_controller::class, 'index']);

// Http requests
Route::post("login", [login_controller::class,'testRequest']);
Route::view("request", 'new_request');

Route::view("homePage", "homePage");

Route::get("request", function() {
    if(session()->has('data'))
    {
        return redirect('homePage');
    }
    return view('new_request');
});

Route::get("logout", function() {
    if(session()->has('data'))
    {
        session()->pull('data');
        return redirect('request');
    }
});

// File upload route
Route::view('upload', 'upload');
Route::post('upload', [upload_controller::class, 'index']);

// Show database table data using model
Route::get('friends', [friends_controller::class, 'show']);