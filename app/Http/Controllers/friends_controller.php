<?php

namespace App\Http\Controllers;
use App\Models\Friend;

use Illuminate\Http\Request;

class friends_controller extends Controller
{
    //
    function show(){
        $data = Friend::all();
        return view('friends', ['friends'=>$data]);
    }
}
