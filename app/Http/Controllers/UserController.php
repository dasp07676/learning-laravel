<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    //
    function loadView($name){
        return view('users',['name'=>$name]);   
    }

    function viewLoad(){
        $data = [1,2,3,4];
        return view('new', ['values'=>$data]);
    }
}
