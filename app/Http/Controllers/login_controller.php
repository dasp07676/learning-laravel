<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class login_controller extends Controller
{
    //
    function testRequest(Request $req){
        // return "Hello I am from login controller";
        // return $req->input();
        $data = $req->input();
        $req->session()->put('data',$data['username']);
        return redirect("homePage");
    }
}
