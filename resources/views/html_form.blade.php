<h1>Form</h1>
<form action="data" method="POST">
    @csrf
    <input type="text" name="username" placeholder="Enter your name here">
    <br>
    <span style="color: red">@error('username'){{$message}}@enderror</span>
    <br>
    <br>
    <input type="password" name="password" placeholder="Enter your password here">
    <br>
    <span style="color: red">@error('password'){{$message}}@enderror</span>
    <br>
    <button type="submit">Login</button>
</form>