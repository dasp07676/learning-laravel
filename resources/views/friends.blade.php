<h1>Friends list</h1>
<table border="1" style="margin:0; text-align:center;">
    <tr>
        <td>Id</td>
        <td>Name</td>
        <td>Address</td>
        <td>Email</td>
    </tr>
    @foreach ($friends as $data)
    <tr>
        <td>{{$data['id']}}</td>
        <td>{{$data['name']}}</td>
        <td>{{$data['address']}}</td>
        <td>{{$data['email']}}</td>
    </tr>
    @endforeach
</table>